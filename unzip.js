var zlib = require('zlib');

module.exports = function(req, res, next) {
  var _write = res.write;
  var _end = res.end;
  var buffers = [];

  res.write = function (data, encoding) {
    buffers.push(data);
    if (!res._headers["content-type"].match(/^text\/html/)) {
      _write.call(res, data, encoding);
    }
  };

  res.end = function (data, encoding) {
    if (data) {
      res.write(data, encoding);
    }
    var buffer = Buffer.concat(buffers);
    if (buffer.length > 0) {
      if (res._headers["content-type"].match(/^text\/html/)) {
        if (res._headers["x-zipped"]) {
          zlib.unzip(buffer, function(err, unzippedBuffer) {
            if (!err) {
              _end.call(res, unzippedBuffer, encoding);
            } else {
              _end.call(res, buffer, encoding);
            }
          });
        } else {
          _end.call(res, buffer, encoding);
        }
      } else {
        _end.call(res, data, encoding);
      }
    } else {
    _end.call(res, data, encoding);
    }
  };
  next();
}


