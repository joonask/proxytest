var express = require('express');
var serveStatic = require('serve-static');

module.exports = function(dir, port) {
  var app = express();
  app.use(serveStatic(dir));
  app.use(function(req, res, next) {
    console.info('request successfully proxied to: ' + req.url + '\n' + JSON.stringify(req.headers, true, 2));
    next();
  });
  app.listen(port);
  console.info("serving " + dir + " at http://localhost:" + port);
}
