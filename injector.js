var Stream = require('stream').Stream;
var iconv = require('iconv-lite');
var injectScript = '<script type="text/javascript" src="inject.js"></script></body>';
var zlib = require('zlib');


module.exports = function(proxyRes) {
  var contentType = proxyRes.headers['content-type'] || '';
  var contentEncoding = proxyRes.headers['content-encoding'] || '';

  if (contentType.match(/^text\/html/)) {
    console.log("Encoding: ", contentEncoding);
    // calc new content-length
    var proxyResPipe = proxyRes.pipe;
    proxyRes.pipe = function(destination) {
      var stream = new Stream();
      var buffers = [];
      stream.writable = true;
      stream.write = function(data) {
       if (data) buffers.push(data);
      };
      stream.end = function(data) {
        if (data) buffers.push(data);
        // Concatenate all buffers
        data = Buffer.concat(buffers);

        if (contentEncoding === 'gzip') {
          console.log("decompressing data");
          function uncompress(data, callback) {
            zlib.unzip(data, callback);
          }
          function recompress(data, callback) {
            zlib.gzip(data, callback);
          }
          uncompress(data, function(err, unCompData) {
            //data = unCompData;
            data = inject(unCompData);
            recompress(data, function(err, compData) {
              destination.end(compData);
            });
          });
        // not gzipped
        } else {
          destination.end(inject(data));
        }

        function inject(data) {
          var charset = 'utf8';
          proxyRes.headers['content-length'] = parseInt(
              proxyRes.headers['content-length']
            ) + Buffer.byteLength(injectScript);
          if (contentType) {
            var m = contentType.match(/charset=([^ ]+)/);
            if (m) charset = m[1];
          }
          var html = iconv.decode(data, charset);
          html = html.replace(/<body>/, '<body>' + injectScript);
          return iconv.encode(html, charset);
        }
      };
      proxyResPipe.call(proxyRes, stream);
    };
  }
}

/**
 * @param userServer
 * @param proxyUrl
 * @returns {{match: RegExp, fn: Function}}
 */
function rewriteLinks(userServer, proxyUrl) {

    var string = "";
    var host = userServer.host;
    var port = userServer.port;

    if (host && port) {
        string = host;
        if (parseInt(port, 10) !== 80) {
            string = host + ":" + port;
        }
    } else {
        string = host;
    }

    return {
        match: new RegExp("['\"]([htps:/]+)?" + string + ".*?(?='|\")", "g"),
        fn: function (match) {
            return match.replace(new RegExp(escapeString(string)), proxyUrl);
        }
    };
}
/**
 * Escape regular expression test string
 *
 * @param str
 * @returns {*|string}
 */
function escapeString(str) {
    return (str+'').replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
};

