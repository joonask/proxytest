var http = require('http');
var httpProxy = require('http-proxy');

var config = {
  // target server for testing
  app: {
    enabled: false,
    dir: 'pub/',
    port: 3000
  },
  proxy: {
    targetHost: 'mtv.fi',
    targetPort: '80',
    targetScheme: 'http',
    port: 4000
  }
};

var proxy = httpProxy.createProxyServer({});

proxy.on('proxyReq', function(proxyReq, req) {
  proxyReq.setHeader('Host', config.proxy.targetHost);
  //proxyReq.setHeader('Accept-Encoding', 'deflate');
  console.log("GET " + req.url);
});
var injector = require('./injector');
proxy.on('proxyRes', injector);




var server = http.createServer(function(req, res) {
  if (req.url === '/inject.js') {
    res.setHeader('Content-Type', 'text/javascript');
    res.end('console.info("injected successfully");');
  } else {
    var target = config.proxy.targetScheme + '://' + config.proxy.targetHost;
    console.info("Proxying target " + target);
    proxy.web(req, res, {
      target: target
    });
  }
});

console.log("listening on port " + config.proxy.port);
server.listen(config.proxy.port);

// test target server
if (config.app.enabled) {
  require('./static')(config.app.dir, config.app.port);
}











